<?php
/**
 * Plugin Name: Hide Articles Content
 * Plugin URI: https://bitbucket.org/grantcardone/hide-articles-content
 * Description: Hides the content for selected articles categories making it available only for selected Groups of users.
 * Version: 1.1.1
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.2.2
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_filter( 'the_content', 'hac_hide_content' );

function hac_hide_content( $content ) {

	$is_article = false;

	$content_blocked = '<div class="row">
	<div class="col-sm-6">
		<h2>This content is for FREE Subscribers only.</h2>
		<a href="/subscribe" class="btn btn-xs">SUBSCRIBE NOW</a>
	</div>
	<div class="col-sm-6">
		<h2>Subscribed Already?</h2>
		<a class="btn btn-xs" href="#" data-toggle="modal" data-target="#myModal2">LOGIN</a>
	</div>
</div>';

	// load all 'category' terms for the post
$terms = get_the_terms( $post->ID, 'category' );

if ( !empty($terms) ) {
	foreach( $terms as $key => $term ) {
		if( $term->slug == 'articles' ) {
			$is_article = true;
			unset($terms[$key]);
		}
	}
}


// we will use the first term to load ACF data from
if( !empty($terms) ) {
	$term = array_pop($terms);
	$availablelist = get_field('available_to', $term );
	$blocked = get_field('blocked', $term );

    // $blocked = get_field('blocked', $term );
	$is_subscriber = false;
	require_once( ABSPATH . 'wp-includes/pluggable.php' );
	if ( $group = Groups_Group::read_by_name( $availablelist[0] ) ) {
		$is_subscriber = Groups_User_Group::read( get_current_user_id() , $group->group_id );
	}
}

if ( $blocked == 'on' && $is_article && !$is_subscriber && has_term( $terms, 'category' ) ) {
	return $content_blocked;
} else {
	return $content;
}

}